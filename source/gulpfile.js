'use strict';

// NPM Dependecies
const gulp = require('gulp');

const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');
const sourcemaps = require('gulp-sourcemaps');

const concat = require('gulp-concat');
const uglify = require('gulp-uglify');


// Config watchers and paths
const paths = {
	dest: '../',
	styles: {
		src: './styles/**/*.scss',
		dest: './../css/'
	},
	javascript : {
		src: [
      './node_modules/jquery/dist/jquery.min.js',
			'./vendors/jQuery.Flipster/jquery.flipster.min.js',
			'./node_modules/slick-carousel/slick/slick.min.js',
			'./vendors/js-cloudimage-360-view/js-cloudimage-360-view.min.js',
			'./javascripts/script.js',
		],
		dest: './../js/'
	}
}


// Task for styles
let styles = () => {
	return gulp.src(paths.styles.src)
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(cleanCSS())
		.pipe(concat('style.min.css'))
		.pipe(sourcemaps.write('./maps'))
		.pipe(gulp.dest(paths.styles.dest));
}

let javascript = () => {
	return gulp.src(paths.javascript.src)
		// .pipe(uglify())
		.pipe(concat('script.min.js'))
		.pipe(gulp.dest(paths.javascript.dest));
}


// Watch
let watch = () => {
	gulp.watch(paths.styles.src, styles);
	gulp.watch(paths.javascript.src, javascript);
}

let build = gulp.series(gulp.parallel(styles, javascript, watch));


gulp.task('build', build);

gulp.task('default', build);