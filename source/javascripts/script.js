let Global = function () {
	'use strict'

  let activeCoverMenu = function ()  {
    $('#menu-icon').on('click', function (){
      $(this).toggleClass('active');
      $('.wrapper-header').toggleClass('menu-active');
      $('.wrapper-menu').toggleClass('active');

      $('.image-logo').toggleClass('inactive');
      $('.icon-logo').toggleClass('active');

      $('.wrapper-contact').toggleClass('active');
    })
  }

  let firstStep = function () {

  }

  let openLightbox = function () {
    $('.open-lightbox').on('click', function(){
      $('.wrapper-lightbox').addClass('active');
      let content = $('.wrapper-lightbox .content');
      let image = $(this).data('image');
      let title = $(this).data('title');
      let description = $(this).data('description');

      if (image === "") {
        $(content).html(`<h4 class="title">${title}</h4><div class="description">${description}</div>`);
      } else {
        $(content).html(`<div class="row"><div class="col-md-7"><div class="image"><img src="${image}" /></div></div><div class="col-md-5"><h4 class="title">${title}</h4><div class="description">${description}</div></div></div>`);
      }
    });
    $('.close-lightbox').on('click', function(){
      $('.wrapper-lightbox').removeClass('active');
    });
  }

  let secondStep = function () {
    $('#start').on('click', function (e) {
        $('.section').removeClass('active')
        $('.section').eq(1).addClass('active')
    })
    let bogotaDealers = '<option value="" selected disabled>&nbsp;</option>' +
        '<option value="Carco" data-phone="3223888294">Carco</option>' +
        '<option value="CVI S.A." data-phone="3102262734">CVI S.A.</option>' +
        '<option value="Distoyota Calle 102" data-phone="3006133278">Distoyota Calle 102</option>' +
        '<option value="Distoyota Calle 13" data-phone="3176441864">Distoyota Calle 13</option>' +
        '<option value="Distoyota Calle 150" data-phone="3104052627">Distoyota Calle 150</option>' +
        '<option value="Novamotors" data-phone="3203851044">Novamotors</option>' +
        '<option value="Toyonorte Calle 127" data-phone="3214913410">Toyonorte Calle 127</option>' +
        '<option value="Yokomotor Calle 134" data-phone="3108669070">Yokomotor Calle 134</option>' +
        '<option value="Yokomotor Calle 72" data-phone="3222169121">Yokomotor Calle 72</option>';

    let medellinDealers = '<option value="" selected disabled>&nbsp;</option>' +
        '<option value="Autoamérica Industriales" data-phone="3208899686">Autoamérica Industriales</option>' +
        '<option value="Autoamérica Palacé" data-phone="3208899686">Autoamérica Palacé</option>' +
        '<option value="Tuyomotor" data-phone="3137507817">Tuyomotor</option>' +
        '<option value="Yokomotor El Tesoro" data-phone="3182370813">Yokomotor El Tesoro</option>' +
        '<option value="Yokomotor Guayabal" data-phone="3217944949">Yokomotor Guayabal</option>' +
        '<option value="Yokomotor San Diego Palacé" data-phone="3128627216">Yokomotor San Diego Palacé</option>';

    let caliDealers = '<option value="" selected disabled>&nbsp;</option>' +
        '<option value="Agricola Automotriz Cali" data-phone="3174039240">Agricola Automotriz Cali</option>' +
        '<option value="Agricola Automotriz Cali C Jardin" data-phone="3174039240">Agricola Automotriz Cali C Jardin</option>' +
        '<option value="Agricola Automotriz Tulua" data-phone="3174039240">Agricola Automotriz Tulua</option>' +
        '<option value="Automotora Norte Y Sur" data-phone="3217007796">Automotora Norte Y Sur</option>';

    let apartadoDealers = '<option value="" selected disabled>&nbsp;</option>' +
        '<option value="Autoamérica Apartado" data-phone="3208899686">Autoamérica Apartado</option>';

    let armeniaDealers = '<option value="" selected disabled>&nbsp;</option>' +
        '<option value="Autocordillera" data-phone="3113211137">Autocordillera</option>';

    let barrancabermejaDealers = '<option value="" selected disabled>&nbsp;</option>' +
        '<option value="Sanautos" data-phone="3203949004">Sanautos</option>';

    let barranquillaDealers = '<option value="" selected disabled>&nbsp;</option>' +
        '<option value="Automercantil Del Caribe Norte" data-phone="3114066588">Automercantil Del Caribe Norte</option>' +
        '<option value="Automercantil Del Caribe Principal" data-phone="3114066588">Automercantil Del Caribe Principal</option>' +
        '<option value="Autotropical B/quilla Country" data-phone="3205116338">Autotropical B/quilla Country</option>' +
        '<option value="Autotropical B/quilla Prado 55" data-phone="3205116338">Autotropical B/quilla Prado 55</option>';

    let bucaramangaDealers = '<option value="" selected disabled>&nbsp;</option>' +
        '<option value="Distoyota Bucaramanga" data-phone="3115623020">Distoyota Bucaramanga</option>';

    let cartagenaDealers = '<option value="" selected disabled>&nbsp;</option>' +
        '<option value="Juanautos El Cerro" data-phone="3057341369">Juanautos El Cerro</option>';

    let cucutaDealers = '<option value="" selected disabled>&nbsp;</option>' +
        '<option value="Cucuta Motors Sas" data-phone="3173719818">Cucuta Motors Sas</option>';

    let laDoradaDealers = '<option value="" selected disabled>&nbsp;</option>' +
        '<option value="Distoyota La Dorada" data-phone="3138893299">Distoyota La Dorada</option>';

    let pereiraDealers = '<option value="" selected disabled>&nbsp;</option>' +
        '<option value="Automotora De Occidente" data-phone="3206321247">Automotora De Occidente</option>' +
        '<option value="Vehiculos Del Café S.a.s" data-phone="3108311106">Vehiculos Del Café S.a.s</option>';

    let manizalesDealers = '<option value="" selected disabled>&nbsp;</option>' +
        '<option value="Vehicaldas" data-phone="3207159031">Vehicaldas</option>';

    let monteriaDealers = '<option value="" selected disabled>&nbsp;</option>' +
        '<option value="Auto Roble Monteria" data-phone="3017523638">Auto Roble Monteria</option>';

    let neivaDealers = '<option value="" selected disabled>&nbsp;</option>' +
        '<option value="Distoyota Neiva" data-phone="3138893318">Distoyota Neiva</option>';

    let pastoDealers = '<option value="" selected disabled>&nbsp;</option>' +
        '<option value="Distoyota Pasto" data-phone="3208798057">Distoyota Pasto</option>';

    let riohachaDealers = '<option value="" selected disabled>&nbsp;</option>' +
        '<option value="Autotropical Riohacha" data-phone="3205116338">Autotropical Riohacha</option>';

    let santaMartaDealers = '<option value="" selected disabled>&nbsp;</option>' +
        '<option value="Autotropical Santa Marta" data-phone="3205116338">Autotropical Santa Marta</option>';

    let sincelejoDealers = '<option value="" selected disabled>&nbsp;</option>' +
        '<option value="Auto Roble Sincelejo" data-phone="3122163707">Auto Roble Sincelejo</option>';

    let ibagueDealers = '<option value="" selected disabled>&nbsp;</option>' +
        '<option value="Distoyota Ibagué" data-phone="3124576287">Distoyota Ibagué</option>';

    let tunjaDealers = '<option value="" selected disabled>&nbsp;</option>' +
        '<option value="Alborautos Tunja" data-phone="3182529773">Alborautos Tunja</option>';

    let valleduparDealers = '<option value="" selected disabled>&nbsp;</option>' +
        '<option value="Autotropical Valledupar" data-phone="3205116338">Autotropical Valledupar</option>';

    let villavicencioDealers = '<option value="" selected disabled>&nbsp;</option>' +
        '<option value="Vehillanos" data-phone="3106195567">Vehillanos</option>';

    let yopalDealers = '<option value="" selected disabled>&nbsp;</option>' +
        '<option value="Alborautos Yopal" data-phone="3182529773">Alborautos Yopal</option>';

    $("#city").change(function () {
      let city = $(this).val();
      if (city === 'bogota') {
        $("#dealer").html(bogotaDealers);
      } else if (city === 'medellin') {
        $("#dealer").html(medellinDealers)
      } else if (city === 'cali') {
        $("#dealer").html(caliDealers)
      } else if (city === 'apartado') {
        $("#dealer").html(apartadoDealers)
      } else if (city === 'armenia') {
        $("#dealer").html(armeniaDealers)
      } else if (city === 'barrancabermeja') {
        $("#dealer").html(barrancabermejaDealers)
      } else if (city === 'barranquilla') {
        $("#dealer").html(barranquillaDealers)
      } else if (city === 'bucaramanga') {
        $("#dealer").html(bucaramangaDealers)
      } else if (city === 'cartagena') {
        $("#dealer").html(cartagenaDealers)
      } else if (city === 'cucuta') {
        $("#dealer").html(cucutaDealers)
      } else if (city === 'la-dorada') {
        $("#dealer").html(laDoradaDealers)
      } else if (city === 'pereira') {
        $("#dealer").html(pereiraDealers)
      } else if (city === 'manizales') {
        $("#dealer").html(manizalesDealers)
      } else if (city === 'monteria') {
        $("#dealer").html(monteriaDealers)
      } else if (city === 'neiva') {
        $("#dealer").html(neivaDealers)
      } else if (city === 'pasto') {
        $("#dealer").html(pastoDealers)
      } else if (city === 'riohacha') {
        $("#dealer").html(riohachaDealers)
      } else if (city === 'santa-marta') {
        $("#dealer").html(santaMartaDealers)
      } else if (city === 'sincelejo') {
        $("#dealer").html(sincelejoDealers)
      } else if (city === 'ibague') {
        $("#dealer").html(ibagueDealers)
      } else if (city === 'tunja') {
        $("#dealer").html(tunjaDealers)
      } else if (city === 'valledupar') {
        $("#dealer").html(valleduparDealers)
      } else if (city === 'villavicencio') {
        $("#dealer").html(villavicencioDealers)
      } else if (city === 'yopal') {
        $("#dealer").html(yopalDealers)
      }
    });

    let phoneDealerSelected = '';

    $("#dealer").change(function () {
      phoneDealerSelected = $(this).find(':selected').data('phone');
    });

    $('#register-form').submit(function (e){
      e.preventDefault();
      if ($("#name").val() === "" || $("#phone").val() === "" || $("#city").val() === null || $("#dealer").val() === null || $('#terms:checked').val() === undefined) {
        $('#register-form > .message-alert').addClass('active');
      } else {
        $('.section').removeClass('active');
        $('.section').eq(2).addClass('active');

        $('.wrapper-dots ul li').removeClass('active');
        $('.wrapper-dots ul li').eq(1).addClass('active')

        $('#wrapper-site').removeClass('bg-one');
        $('#wrapper-site').addClass('bg-two');

        $('#menu-icon').addClass('show');

        $('.wrapper-contact').addClass('active');

        let pathWhatsapp = `https://wa.me/57${phoneDealerSelected}/?text=Hola,%20Me%20interesa%20recibir%20asesoria.`;
        $('.whatsapp-cta a').attr('href', pathWhatsapp)
        $('.link-chat-whatsapp').attr('href', pathWhatsapp)
      }
    });

    $("input").focus(function() {
      $('.message-alert').removeClass('active');
    });
    $("select").change(function () {
      $('.message-alert').removeClass('active');
    });
  }

  let thirdStep = function () {
    var song = '';
    $('.load-cars').on('click', function(e){
      e.preventDefault();
      let genreSelected = $(this).attr('id');
      console.log(genreSelected);
      if (genreSelected === 'rock') {
        let rockSongs = ['./audio/rock-1.mp3', './audio/rock-2.mp3'];
        let rockSongRandom = rockSongs[Math.floor(Math.random() * rockSongs.length)];
        song = new Audio(rockSongRandom);
      } else if(genreSelected === 'jazz') {
        let jazzSongs = ['./audio/jazz-1.mp3', './audio/jazz-2.mp3'];
        let jazzSongRandom = jazzSongs[Math.floor(Math.random() * jazzSongs.length)];
        song = new Audio(jazzSongRandom);
      } else if(genreSelected === 'chillout') {
        let chilloutSongs = ['./audio/chillout-1.mp3', './audio/chillout-2.mp3'];
        let chilloutSongRandom = chilloutSongs[Math.floor(Math.random() * chilloutSongs.length)];
        song = new Audio(chilloutSongRandom);
      } else if(genreSelected === 'electro-swing') {
        let electroSwingSongs = ['./audio/electro-swing-1.mp3', './audio/electro-swing-2.mp3'];
        let electroSwingSongRandom = electroSwingSongs[Math.floor(Math.random() * electroSwingSongs.length)];
        song = new Audio(electroSwingSongRandom);
      }
      song.loop = true;
      song.play();
      $('.section').removeClass('active');
      $('.section').eq(3).addClass('active');

      $('.wrapper-dots ul li').removeClass('active');
      $('.wrapper-dots ul li').eq(2).addClass('active');

      $('#sound').addClass('show');
    });
    $('#sound').on('click', function(){
      $(this).toggleClass('off');
      if($(this).hasClass('off')) {
        $('.volume').removeClass('icon-volume-active');
        $('.volume').addClass('icon-volume-inactive');
        song.pause();
      } else {
        $('.volume').removeClass('icon-volume-inactive');
        $('.volume').addClass('icon-volume-active');
        song.play();
      }
    });
    $('.link-select-music, .back-music').on('click', function(e){
      e.preventDefault();

      $('#menu-icon').removeClass('active');
      $('.wrapper-header').removeClass('menu-active');
      $('.wrapper-menu').removeClass('active');
      $('.image-logo').removeClass('inactive');
      $('.icon-logo').removeClass('active');

      $('.section').removeClass('active');
      $('.section').eq(2).addClass('active');
      song.pause();
      song.currentTime = 0;
    });
    $('.link-select-car, .back-cars').on('click', function(e){
      e.preventDefault();

      $('#menu-icon').removeClass('active');
      $('.wrapper-header').removeClass('menu-active');
      $('.wrapper-menu').removeClass('active');
      $('.image-logo').removeClass('inactive');
      $('.icon-logo').removeClass('active');

      $('.section').removeClass('active');
      $('.section').eq(3).addClass('active');
    });
  }


  let fourthStep = function () {
    $('#flipster-cars').flipster({
      buttons: true,
      loop: true,
      keyboard: true,
      style: 'carousel',
      spacing: -0.5,
    });
    $('.select-cart').on('click', function(e){
      let carSelected = $(this).data('car');
      console.log(carSelected);
      $('.section').removeClass('active');
      $('.section.car-' + carSelected).addClass('active');

      $('.wrapper-dots ul li').removeClass('active');
      $('.wrapper-dots ul li').eq(3).addClass('active');
    })
  }

  let fifthStep = function () {
    $('.carousel-car').slick({
      draggable: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      prevArrow: '<i class="icon-arrow-left slick-prev"></i>',
      nextArrow: '<i class="icon-arrow-right slick-next"></i>',
      infinite: false
    });

    $('.colors-selector .color').on('click', function () {
      let color = $(this).data('color');
      $('.colors-selector .color').removeClass('active');
      $(this).addClass('active');
      let image = $(this).parent().parent().find('.image-car-color');
      let name = $(this).parent().parent().find('.name-color');
      let folder = $(image).data('folder');
      let nameColor = $(this).data('name');
      let path = `./images/cars/${folder}/colores/${color}.png`;
      $(image).attr('src', path);
      $(name).html(nameColor);
    });

    $('.open-gallery-inside').on('click', function () {
      let gallery = $(this).data('gallery');
      $('.wrapper-lightbox').removeClass('active');
      $('.wrapper-gallery-inside').addClass('active')
      $('.wrapper-gallery-inside .gallery.'+ gallery).addClass('active')
    });
    $('.open-gallery-outside').on('click', function () {
      let gallery = $(this).data('gallery');
      $('.wrapper-lightbox').removeClass('active');
      $('.wrapper-gallery-outside').addClass('active')
      $('.wrapper-gallery-outside .gallery.'+ gallery).addClass('active')
    });
    $('.back-details').on('click', function () {
      $('.wrapper-gallery-inside, .wrapper-gallery-outside').removeClass('active')
      $('.wrapper-gallery-inside .gallery, .wrapper-gallery-outside .gallery').removeClass('active')
      $('.wrapper-lightbox').removeClass('active');
    })

    $('.wrapper-gallery-outside .gallery').slick({
      draggable: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      prevArrow: '<i class="icon-arrow-left slick-prev"></i>',
      nextArrow: '<i class="icon-arrow-right slick-next"></i>',
    });
  }

	return {
		init: function () {
      activeCoverMenu();
      openLightbox();
      firstStep();
      secondStep();
      thirdStep();
      fourthStep();
      fifthStep();
		}
	}
}()

$(document).ready(function() {
	Global.init();
})